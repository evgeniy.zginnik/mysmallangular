(function() {

  function smallAngular() {

    const rootScope = {};
    const directives = {};

    function directive(name, fn) {
      directives[name] = fn;

      return this;
    }

    const compile = node => {

    }

    rootScope.$apply = () => {

    }

    this.createApp = appName => {
      this.appName = appName;
      return this;
    }

    this.$whatch = () => {

    }

    this.bootstrap = (node = document.querySelector('ng-app')) => {

    }

    this.directive = directive;
    // this.controller = controller;
    // this.servise = servise;
    // initBaseDirectives();
    this.bootstrap();
  }

  window.angular = new smallAngular();

})()
